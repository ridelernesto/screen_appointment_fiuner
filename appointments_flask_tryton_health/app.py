import os

from flask import Flask, flash, render_template, url_for, request, redirect, session, abort
from flask_tryton import Tryton
from flask_babel import Babel
from flask_wtf import FlaskForm
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta

from functools import wraps

from wtforms import StringField
from wtforms.validators import DataRequired
from wtforms.fields import PasswordField

import time 
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
#from selenium.common.keys import Keys

app = Flask(__name__)
babel = Babel(app)
app.config['TRYTON_DATABASE'] = 'healthx'
#os.environ.get('pruebas', 'pruebas')
app.config['TRYTON_CONFIG'] = '/home/ernesto/.virtualenvs/healthx/lib/python3.8/site-packages/trytond/etc/trytond.conf'
tryton = Tryton(app, configure_jinja=True)


SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
turnos_webhook = []
"""WebUser = tryton.pool.get('web.user')
Session = tryton.pool.get('web.user.session')"""


#Obtenemos los modelos
Appointments = tryton.pool.get('gnuhealth.appointment')
Prescriptions = tryton.pool.get('gnuhealth.prescription.order')
NursingAtentions =  tryton.pool.get('gnuhealth.patient.ambulatory_care')
Evaluations = tryton.pool.get('gnuhealth.patient.evaluation')
Institutions = tryton.pool.get('gnuhealth.institution')
OdontologyTreatments = tryton.pool.get('gnuhealth.dentistry.treatment')
Patients = tryton.pool.get('gnuhealth.patient')
ImagingResults = tryton.pool.get('gnuhealth.imaging.test.result')
LabResults = tryton.pool.get('gnuhealth.lab')
PrenatalEvaluations = tryton.pool.get('gnuhealth.patient.prenatal.evaluation')
CovidValues = tryton.pool.get('gnuhealth.contact_tracing')
EnoValues = tryton.pool.get('gnuhealth.contact_tracing.eno')
ContactTracingCall = tryton.pool.get('gnuhealth.contact_tracing_call')


SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
turnos_webhook = []
institution=['']


#Panel de indicadores

@app.route('/dashboard', methods=['GET','POST'])
@tryton.transaction()
def dashboard_indicators():

    start = datetime.today().replace(hour=0,minute=0,second=0)
    final = datetime.today().replace(hour=23,minute=59,second=59)
    start_d = start.date()

    appointments_count = 0
    prescriptions_count = 0
    evaluations_count = 0
    nursing_ambulatory_care_count = 0
    odontology_treatments_count = 0
    prenatal_evaluations_count = 0
    imaging_results_count = 0
    lab_results_count =  0

#Obtenemos la institución

    institutions = Institutions.search([])
    
    organization = institutions[0].name.name


# Calculamos los turnos presentes del día cuando el estado es "checked_in" o "done"
    
    today = datetime.today().date().strftime('%d %m %Y')
    appointments = Appointments.search([
            ('appointment_date','>=',start),
            ('appointment_date','<',final),
            ])
    
    for x in appointments:
        if x.state == 'done' or x.state == 'checked_in':
            appointments_count = appointments_count + 1
 
# Calculamos las evaluaciones realizadas en el día -> estado  "done"
 
    evaluations = Evaluations.search([
            ('evaluation_start','>=',start),
            ('evaluation_start','<',final),
            ])
    
    for x in evaluations:
        if x.state == 'signed' :
            evaluations_count = evaluations_count + 1


# Calculamos las prescripciones realizadas en el día -> estado  "done"
 
    prescriptions = Prescriptions.search([
            ('prescription_date','>=',start),
            ('prescription_date','<',final),
            ])
    
    for x in prescriptions:
        if x.state == 'done' :
            prescriptions_count = prescriptions_count + 1
            
# Calculamos las atenciones ambulatorias de enfermería realizadas en el día -> estado  "done"
 
    ambulatory_cares = NursingAtentions.search([
            ('session_start','>=',start),
            ('session_start','<',final),
            ])
    
    for x in ambulatory_cares:
        if x.state == 'done' :
            nursing_ambulatory_care_count = nursing_ambulatory_care_count + 1

# Calculamos las atenciones odontológicas realizadas en el día -> estado  "done"
 
    odontology_treatments = OdontologyTreatments.search([
            ('treatment_date','=',start_d),
            ])
    
    for x in odontology_treatments:
        if x.state == 'done' :
            odontology_treatments_count = odontology_treatments_count + 1
            
# Calculamos las evaluaciones prenatales realizadas en el día -> estado  "done"
 
    prenatal_evaluations = PrenatalEvaluations.search([
            ('evaluation_date','>=',start),
            ('evaluation_date','<',final)
            ])
    
    for x in prenatal_evaluations:
        prenatal_evaluations_count = prenatal_evaluations_count + 1

# Calculamos los pacientes nuevos registrados en el día
 
    imaging_results = ImagingResults.search([
            ('date','>=',start),
            ('date','<',final)
            ])
    
    for x in imaging_results:
        imaging_results_count = imaging_results_count + 1
        
# Calculamos los laboratorios realizados
 
    lab_results = LabResults.search([
            ('date_analysis','>=',start),
            ('date_analysis','<',final)
            ])
    
    for x in lab_results:
        lab_results_count = lab_results_count + 1

# Calculamos los laboratorios realizados
 
    total_follow_covid = CovidValues.search([])

    today_follow_covid = CovidValues.search([
        ('discharge','=',False)])

    total_confirmed_cases = CovidValues.search([
        ('epidemiological_condition','=', 'confirmed')])

    today_confirmed_cases = CovidValues.search([
            ('first_contact','>=',start),
            ('first_contact','<',final),
            ('epidemiological_condition','=', 'confirmed')
            ])
    
    active_cases = len(today_confirmed_cases)
    follow_cases = len(today_follow_covid)
    total_cases = len(total_confirmed_cases)
    total_follow = len(total_follow_covid)

    return render_template('dashboard.html',
                           today=today,
                           appointments_count = appointments_count,
                           prescriptions_count = prescriptions_count,
                           evaluations_count = evaluations_count,
                           nursing_ambulatory_care_count = nursing_ambulatory_care_count,
                           organization = organization,
                           odontology_treatments_count = odontology_treatments_count,
                           prenatal_evaluations_count = prenatal_evaluations_count,
                           imaging_results_count = imaging_results_count,
                           lab_results_count = lab_results_count,
                           active_cases = active_cases, follow_cases = follow_cases, total_cases = total_cases, total_follow = total_follow
                            )

# Panel de los turnos
@app.route('/dashboard_turnos', methods=['GET','POST'])
@tryton.transaction()
def dashboard_turnos():
    #Obtenemos la institución

    institutions = Institutions.search([])
    
    organization = institutions[0].name.name

    appointments = Appointments.search([])

    categories = []
    values_categories_men = {}
    values_categories_woman = {}

    for appointment in appointments:
        if appointment.speciality and appointment.healthprof:
            if appointment.speciality.name not in categories:
                categories.append(appointment.speciality.name)
                values_categories_woman[appointment.speciality.name]=0
                values_categories_men[appointment.speciality.name]=0
            if appointment.patient.gender == 'f':
                values_categories_woman[appointment.speciality.name]+=1
            elif appointment.patient.gender == 'm':
                values_categories_men[appointment.speciality.name]+=1

    data = {
        'categories': categories,
        'values_categories_men': list(values_categories_men.values()),
        'values_categories_woman': list(values_categories_woman.values())
    }    
    return render_template('dashboard_turnos.html',
                            data=data)

# Panel de Covid
@app.route('/dashboard_covid', methods=['GET','POST'])
@tryton.transaction()
def dashboard_covid():
    #Obtenemos la institución
    final = datetime.today().replace(hour=23,minute=59,second=59)

    institutions = Institutions.search([])
    
    organization = institutions[0].name.name

    # Calculamos los laboratorios realizados
    start_date_count = datetime(2020, 3, 1, 0, 0)
    contact_covid = CovidValues.search([
        ('first_contact','>=', start_date_count),
        ('first_contact','<', final)])

    diferencia = relativedelta(final,start_date_count)
    meses = diferencia.years*12 + diferencia.months

    dates = []
    for i in range(0, meses+1):
        dates.append(str(start_date_count.month) + "-" + str(start_date_count.year))
        start_date_count = start_date_count + relativedelta(months=1)

    values = [0]*(meses+1)

    total_state_person_covid = ["Recuperado",
                                "Hospitalizado",
                                "Descartado por laboratorio",
                                "Contacto perdido",
                                "Persona fallecida",
                                "Contacto estreco asintomatico",
                                "Sin especificar"]

    values_total_state_person_covid = [0]*len(total_state_person_covid)

    total_discharges = 0

    confirmed_cases = [0]*(meses+1)
    total_cases = 0

    # obtengo los valores para las graficas
    for contact in contact_covid:
        diferencia = relativedelta(final, contact.first_contact)
        index = diferencia.years*12 + diferencia.months
        values[index]+=1 

        if contact.epidemiological_condition == 'confirmed':
            confirmed_cases[index]+=1
            total_cases+=1

        if contact.discharge == True:
            total_discharges+=1
            if contact.result == 'r':
                values_total_state_person_covid[0]+=1
            elif contact.result == 'h':
                values_total_state_person_covid[1]+=1  
            elif contact.result == 'dl':
                values_total_state_person_covid[2]+=1       
            elif contact.result == 'l':
                values_total_state_person_covid[3]+=1   
            elif contact.result == 'd':
                values_total_state_person_covid[4]+=1
            elif contact.result == 'cca':
                values_total_state_person_covid[5]+=1        
            elif contact.result == None:
                values_total_state_person_covid[6]+=1

    total_follow = len(contact_covid)
    time_total_follow = str(meses) + " meses"
    values.reverse() 
    confirmed_cases.reverse()
    
    contact_calls = ContactTracingCall.search([])

    values_calls = [0]*(meses+1)
    for calls in contact_calls:
        diferencia = relativedelta(final, calls.date)
        index = diferencia.years*12 + diferencia.months
        values_calls[index]+=1 
    values_calls.reverse()

    total_calls = len(contact_calls)

    data = {
        'axisx': dates,
        'axisy1':values,
        'categories_result':total_state_person_covid,
        'values_result':values_total_state_person_covid,
        'values_calls':values_calls,
        'confirmed_cases':confirmed_cases
    }

    return render_template('dashboard_covid.html',
                            data=data,
                            total_follow=total_follow, time_total_follow=time_total_follow,
                            total_discharges=total_discharges,
                            total_calls=total_calls, 
                            total_cases=total_cases)    


# Panel de Covid
@app.route('/dashboard_enfermeria', methods=['GET','POST'])
@tryton.transaction()
def dashboard_enfermeria():

    ambulatory_nursing = NursingAtentions.search([])

    data = {
        'atentions':[]
    }
    for ambulatory in ambulatory_nursing:
        atention = {
            'gender': ambulatory.patient.gender,
            'atention_date': str(ambulatory.session_start.day)+"/"+str(ambulatory.session_start.month)+"/"+str(ambulatory.session_start.year),
            'age': relativedelta(ambulatory.session_start,ambulatory.patient.name.dob).years
        }
        data['atentions'].append(atention)

    print(data)    

    return render_template('dashboard_enfermeria.html',
                            data=data)

#Turnos en Pantalla: Mostramos los turnos que están siendo llamado, en atención y los siguientes

@app.route('/turnos', methods=['GET','POST'])
@tryton.transaction()
def webhook():

#Obtenemos la institución

    #institutions = Institutions.search([])
    
    #organization = institutions[0].name.name


    #-- desde aca trabajamos con webhooks
    institution = ''
    recibido = request.json 
    if recibido != None:
        appointment_calling = recibido['appointment']
        institution = recibido['institution']

        count=0

        for i in range(0,len(turnos_webhook)):

            print(turnos_webhook[i])
            print("\n\n\n")    

            if appointment_calling.get('professional') == turnos_webhook[i].get('professional'):
                count+=1
                if appointment_calling.get('patient') == turnos_webhook[i].get('patient'): 
                    if appointment_calling.get('last') == 0:
                        turnos_webhook.pop(i)
                    else:    
                        turnos_webhook[i]=appointment_calling  
                else:                           
                    turnos_webhook.pop(i)
                    turnos_webhook.append(appointment_calling)

        if count == 0:
            turnos_webhook.append(appointment_calling)     

  
    today = datetime.today().date().strftime('%d %m %Y')
    return render_template('appointments.html',
                           today = today,
                           turnos_webhook = turnos_webhook,
                           organization = institution,
                        )

if __name__ == "__main__":
    app.run(debug=True)