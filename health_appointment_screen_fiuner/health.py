#-*- coding: utf-8 -*-

import re
import requests 
import json

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Or, Eval, Not, Bool, Equal
import calendar

__all__ = ['AppointmentData']


class AppointmentData(metaclass = PoolMeta):    
    'Appointment Data'
    __name__ = 'gnuhealth.appointment'

    calling = fields.Boolean("calling")

    #next_ = fields.Boolean("Next")

    in_atention = fields.Boolean("In atention")

    @staticmethod
    def default_calling():
        return False

    @staticmethod
    def default_in_atention():
        return False    

    @classmethod
    def __setup__(cls):
        super(AppointmentData, cls).__setup__()
        cls._order.insert(0, ('appointment_date', 'DESC'))

        cls._buttons.update({
            'checked_in': {'invisible': Not(Equal(Eval('state'), 'confirmed'))}
            })

        cls._buttons.update({
            'no_show': {'invisible': Not(Equal(Eval('state'), 'confirmed'))}
            })

        cls._buttons.update({
            'call': {'visible': Not( Equal(Eval('state'), 'confirmed'))}})

        cls._buttons.update({
            'in_atention': {'visible': Not( Equal(Eval('state'), 'confirmed'))}})


    @classmethod
    @ModelView.button
    def call(cls, AppointmentData):
        pool = Pool()
        Appointments = pool.get('gnuhealth.appointment')
        User = Pool().get('res.user')
        user = User(Transaction().user)

        start = datetime.today().replace(hour=0,minute=0,second=0)
        final = datetime.today().replace(hour=23,minute=59,second=59)
        appointments = Appointments.search([
         	('appointment_date','>=',start),
            ('appointment_date','<',final),
            ('healthprof.id','=',AppointmentData[0].healthprof.id)
            ])


        #Appointments.write(appointments, {'calling':False,
        #	'in_atention':False})

        #cls.write(AppointmentData, {'calling': True})

        if AppointmentData[0].healthprof.name.internal_user.id == user.id:
            last_appointment = 1

            appointment_calling =   {'patient':AppointmentData[0].patient.name.name+", "+AppointmentData[0].patient.name.lastname,
                            'professional':AppointmentData[0].healthprof.name.name+", "+AppointmentData[0].healthprof.name.lastname,
                            'specialty': AppointmentData[0].healthprof.main_specialty.specialty.name,
                            'time':AppointmentData[0].appointment_date.strftime('%H:%M'),
                            'state':'calling',
                            'last':last_appointment
                            }


            appointments_data = {   'appointment':appointment_calling,
                                    'institution':AppointmentData[0].institution.name.name
                                    }

            print(appointments_data)
            print("\n\n\n")                        

            webhook_url = "http://localhost:5000/turnos"
            r = requests.post(webhook_url, data=json.dumps(appointments_data), headers={'Content-Type':'application/json'})

        else:
            cls.raise_user_error('Cada profesional solo puede llamar a sus pacientes')
     
    @classmethod
    @ModelView.button
    def in_atention(cls, AppointmentData):
        pool = Pool()
        Appointments = pool.get('gnuhealth.appointment')
        User = Pool().get('res.user')
        user = User(Transaction().user)
        
        start = datetime.today().replace(hour=0,minute=0,second=0)
        final = datetime.today().replace(hour=23,minute=59,second=59)
        appointments = Appointments.search([
                                                ('appointment_date','>=',start),
                                                ('appointment_date','<',final),
                                                ('healthprof.name.internal_user.id','=',user.id),
                                                ('state','=','confirmed')
                                                ])


        if AppointmentData[0].healthprof.name.internal_user.id == user.id:
            cls.write(AppointmentData, {'state': 'checked_in'})
        
            last_appointment = 1;
            if len(appointments) == 1:
                last_appointment = 0

            appointment_calling = {'patient':AppointmentData[0].patient.name.name+", "+AppointmentData[0].patient.name.lastname,
                            'professional':AppointmentData[0].healthprof.name.name+", "+AppointmentData[0].healthprof.name.lastname,
                            'specialty': AppointmentData[0].healthprof.main_specialty.specialty.name,
                            'time':AppointmentData[0].appointment_date.strftime('%H:%M'),
                            'state':'in_atention',
                            'last':last_appointment
                            }

            appointments_data = {   'appointment':appointment_calling,
                                    'institution':AppointmentData[0].institution.name.name
                                    }

            webhook_url = "http://localhost:5000/turnos"
            r = requests.post(webhook_url, data=json.dumps(appointments_data), headers={'Content-Type':'application/json'})
        else:
            cls.raise_user_error('Cada profesional solo puede llamar a sus pacientes')

























































"""for x in appointment_reset:
            if x.in_atention == True:
                aux = [x]
                Appointments.write(aux, {'in_atention':False})

        if appointment is not None:
            cls.write(appointment, {'in_atention': True})"""